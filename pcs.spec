Name: pcs
Version: 0.10.8
Release: 5%{?dist}
# https://docs.fedoraproject.org/en-US/packaging-guidelines/LicensingGuidelines/
# https://fedoraproject.org/wiki/Licensing:Main?rd=Licensing#Good_Licenses
# GPLv2: pcs
# ASL 2.0: tornado
# MIT: backports, dacite, daemons, ember, ethon, handlebars, jquery, jquery-ui,
#      mustermann, rack, rack-protection, rack-test, sinatra, tilt
# GPLv2 or Ruby: eventmachne, json
# (GPLv2 or Ruby) and BSD: thin
# BSD or Ruby: open4, rexml, ruby2_keywords, webrick
# BSD and MIT: ffi
License: GPLv2 and ASL 2.0 and MIT and BSD and (GPLv2 or Ruby) and (BSD or Ruby)
URL: https://github.com/ClusterLabs/pcs
Group: System Environment/Base
Summary: Pacemaker Configuration System
#building only for architectures with pacemaker and corosync available
ExclusiveArch: i686 x86_64 s390x ppc64le aarch64

%global version_or_commit %{version}
# %%global version_or_commit 508b3999eb02b4901e83b8e780af8422b522ad30

%global pcs_source_name %{name}-%{version_or_commit}

# ui_commit can be determined by hash, tag or branch
%global ui_commit 0.1.5
%global ui_modules_version 0.1.5
%global ui_src_name pcs-web-ui-%{ui_commit}

%global pcs_snmp_pkg_name  pcs-snmp

%global pyagentx_version   0.4.pcs.2
%global tornado_version    6.1.0
%global dacite_version  1.6.0
%global version_rubygem_backports  3.17.2
%global version_rubygem_daemons  1.3.1
%global version_rubygem_ethon  0.12.0
%global version_rubygem_eventmachine  1.2.7
%global version_rubygem_ffi  1.13.1
%global version_rubygem_json  2.3.0
%global version_rubygem_mustermann  1.1.1
%global version_rubygem_open4  1.3.4
%global version_rubygem_rack  2.2.3
%global version_rubygem_rack_protection  2.0.8.1
%global version_rubygem_rack_test  1.1.0
%global version_rubygem_rexml  3.2.4
%global version_rubygem_ruby2_keywords  0.0.2
%global version_rubygem_sinatra  2.0.8.1
%global version_rubygem_thin  1.7.2
%global version_rubygem_tilt  2.0.10
%global version_rubygem_webrick  1.7.0

# javascript bundled libraries for old web-ui
%global ember_version 1.4.0
%global handlebars_version 1.2.1
%global jquery_ui_version 1.10.1
%global jquery_version 1.9.1

# We do not use _libdir macro because upstream is not prepared for it.
# Pcs does not include binaries and thus it should live in /usr/lib. Tornado
# and gems include binaries and thus it should live in /usr/lib64. But the
# path to tornado/gems is hardcoded in pcs sources. Modify hard links in pcs
# sources is not the way since then rpmdiff complains that the same file has
# different content in different architectures.
%global pcs_libdir %{_prefix}/lib
%global bundled_src_dir pcs/bundled
%global pcsd_public_dir pcsd/public
%global rubygem_cache_dir pcsd/vendor/cache
%global rubygem_bundle_dir pcsd/vendor/bundle/ruby

# mangling shebang in /usr/lib/pcsd/vendor/bundle/ruby/gems/rack-2.0.5/test/cgi/test from /usr/bin/env ruby to #!/usr/bin/ruby
#*** ERROR: ./usr/lib/pcsd/vendor/bundle/ruby/gems/rack-2.0.5/test/cgi/test.ru has shebang which doesn't start with '/' (../../bin/rackup)
#mangling shebang in /usr/lib/pcsd/vendor/bundle/ruby/gems/rack-2.0.5/test/cgi/rackup_stub.rb from /usr/bin/env ruby to #!/usr/bin/ruby
#*** WARNING: ./usr/lib/pcsd/vendor/bundle/ruby/gems/rack-2.0.5/test/cgi/sample_rackup.ru is executable but has empty or no shebang, removing executable bit
#*** WARNING: ./usr/lib/pcsd/vendor/bundle/ruby/gems/rack-2.0.5/test/cgi/lighttpd.conf is executable but has empty or no shebang, removing executable bit
#*** ERROR: ambiguous python shebang in /usr/lib/pcsd/vendor/bundle/ruby/gems/ffi-1.9.25/ext/ffi_c/libffi/generate-darwin-source-and-headers.py: #!/usr/bin/env python. Change it to python3 (or python2) explicitly.
%undefine __brp_mangle_shebangs

# https://fedoraproject.org/wiki/Changes/Avoid_usr_bin_python_in_RPM_Build#Python_bytecompilation
# Enforce python3 because bytecompilation of tornado produced warnings:
# DEPRECATION WARNING: python2 invoked with /usr/bin/python.
#    Use /usr/bin/python3 or /usr/bin/python2
#    /usr/bin/python will be removed or switched to Python 3 in the future.
%global __python %{__python3}

Source0: %{url}/archive/%{version_or_commit}/%{pcs_source_name}.tar.gz
Source1: HAM-logo.png
Source2: pcsd-bundle-config-2

Source41: https://github.com/ondrejmular/pyagentx/archive/v%{pyagentx_version}/pyagentx-%{pyagentx_version}.tar.gz
Source42: https://github.com/tornadoweb/tornado/archive/v%{tornado_version}/tornado-%{tornado_version}.tar.gz
Source44: https://github.com/konradhalas/dacite/archive/v%{dacite_version}/dacite-%{dacite_version}.tar.gz

Source81: https://rubygems.org/downloads/backports-%{version_rubygem_backports}.gem
Source82: https://rubygems.org/downloads/ethon-%{version_rubygem_ethon}.gem
Source83: https://rubygems.org/downloads/ffi-%{version_rubygem_ffi}.gem
Source84: https://rubygems.org/downloads/json-%{version_rubygem_json}.gem
Source86: https://rubygems.org/downloads/mustermann-%{version_rubygem_mustermann}.gem
# We needed to re-upload open4 rubygem because of issues with sources in gating.
# Unfortunately, there was no newer version available, therefore we had to
# change its 'version' ourselves.
Source87: https://rubygems.org/downloads/open4-%{version_rubygem_open4}.gem#/open4-%{version_rubygem_open4}-1.gem
Source88: https://rubygems.org/downloads/rack-%{version_rubygem_rack}.gem
Source89: https://rubygems.org/downloads/rack-protection-%{version_rubygem_rack_protection}.gem
Source90: https://rubygems.org/downloads/rack-test-%{version_rubygem_rack_test}.gem
Source91: https://rubygems.org/downloads/sinatra-%{version_rubygem_sinatra}.gem
Source92: https://rubygems.org/downloads/tilt-%{version_rubygem_tilt}.gem
Source93: https://rubygems.org/downloads/eventmachine-%{version_rubygem_eventmachine}.gem
Source94: https://rubygems.org/downloads/daemons-%{version_rubygem_daemons}.gem
Source95: https://rubygems.org/downloads/thin-%{version_rubygem_thin}.gem
Source96: https://rubygems.org/downloads/ruby2_keywords-%{version_rubygem_ruby2_keywords}.gem
Source97: https://rubygems.org/downloads/rexml-%{version_rubygem_rexml}.gem
Source98: https://rubygems.org/downloads/webrick-%{version_rubygem_webrick}.gem

Source100: https://github.com/idevat/pcs-web-ui/archive/%{ui_commit}/%{ui_src_name}.tar.gz
Source101: https://github.com/idevat/pcs-web-ui/releases/download/%{ui_modules_version}/pcs-web-ui-node-modules-%{ui_modules_version}.fix.1.tar.xz

# Patches from upstream.
# They should come before downstream patches to avoid unnecessary conflicts.
# Z-streams are exception here: they can come from upstream but should be
# applied at the end to keep z-stream changes as straightforward as possible.
# Patch1: bzNUMBER-01-name.patch
Patch1: bz1927404-01-replace-pyOpenSSL-with-python-crypt.patch

# Downstream patches do not come from upstream. They adapt pcs for specific
# RHEL needs.
Patch101: do-not-support-cluster-setup-with-udp-u-transport.patch

# git for patches
BuildRequires: git-core
#printf from coreutils is used in makefile
BuildRequires: coreutils
# python for pcs
BuildRequires: python3 >= 3.6
BuildRequires: python3-cryptography
BuildRequires: python3-dateutil >= 2.7.0
BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: python3-pycurl
BuildRequires: python3-pyparsing
# gcc for compiling custom rubygems
BuildRequires: gcc
BuildRequires: gcc-c++
# ruby and gems for pcsd
BuildRequires: ruby >= 2.2.0
BuildRequires: ruby-devel
BuildRequires: rubygems
# ruby libraries for tests
BuildRequires: rubygem-test-unit
# for touching patch files (sanitization function)
BuildRequires: diffstat
# for post, preun and postun macros
BuildRequires: systemd
# for tests
BuildRequires: python3-lxml
# pcsd fonts and font management tools for creating symlinks to fonts
BuildRequires: fontconfig
BuildRequires: liberation-sans-fonts
BuildRequires: make
BuildRequires: overpass-fonts
# Red Hat logo for creating symlink of favicon
BuildRequires: redhat-logos

# for building web ui
BuildRequires: npm

# python and libraries for pcs, setuptools for pcs entrypoint
Requires: python3 >= 3.6
Requires: python3-cryptography
Requires: python3-dateutil >= 2.7.0
Requires: python3-lxml
Requires: python3-setuptools
Requires: python3-pycurl
Requires: python3-pyparsing
# ruby and gems for pcsd
Requires: ruby >= 2.2.0
Requires: rubygems
# for killall
Requires: psmisc
# cluster stack and related packages
Requires: pcmk-cluster-manager >= 2.0.0
Suggests: pacemaker
Requires: (corosync >= 2.99 if pacemaker)
# pcs enables corosync encryption by default so we require libknet1-plugins-all
Requires: (libknet1-plugins-all if corosync)
Requires: pacemaker-cli >= 2.0.0
# for post, preun and postun macros
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
# pam is used for authentication inside daemon (python ctypes)
# more details: https://bugzilla.redhat.com/show_bug.cgi?id=1717113
Requires: pam
# pcsd fonts
Requires: liberation-sans-fonts
Requires: overpass-fonts
# favicon Red Hat logo
Requires: redhat-logos
# needs logrotate for /etc/logrotate.d/pcsd
Requires: logrotate

Provides: bundled(tornado) = %{tornado_version}
Provides: bundled(dacite) = %{dacite_version}
Provides: bundled(backports) = %{version_rubygem_backports}
Provides: bundled(daemons) = %{version_rubygem_daemons}
Provides: bundled(ethon) = %{version_rubygem_ethon}
Provides: bundled(eventmachine) = %{version_rubygem_eventmachine}
Provides: bundled(ffi) = %{version_rubygem_ffi}
Provides: bundled(json) = %{version_rubygem_json}
Provides: bundled(mustermann) = %{version_rubygem_mustermann}
Provides: bundled(open4) = %{version_rubygem_open4}
Provides: bundled(rack) = %{version_rubygem_rack}
Provides: bundled(rack_protection) = %{version_rubygem_rack_protection}
Provides: bundled(rack_test) = %{version_rubygem_rack_test}
Provides: bundled(rexml) = %{version_rubygem_rexml}
Provides: bundled(ruby2_keywords) = %{version_rubygem_ruby2_keywords}
Provides: bundled(sinatra) = %{version_rubygem_sinatra}
Provides: bundled(thin) = %{version_rubygem_thin}
Provides: bundled(tilt) = %{version_rubygem_tilt}
Provides: bundled(webrick) = %{version_rubygem_webrick}

# javascript bundled libraries for old web-ui
Provides: bundled(ember) = %{ember_version}
Provides: bundled(handlebars) = %{handlebars_version}
Provides: bundled(jquery) = %{jquery_version}
Provides: bundled(jquery-ui) = %{jquery_ui_version}

%description
pcs is a corosync and pacemaker configuration tool.  It permits users to
easily view, modify and create pacemaker based clusters.

# pcs-snmp package definition
%package -n %{pcs_snmp_pkg_name}
Group: System Environment/Base
Summary: Pacemaker cluster SNMP agent
# https://fedoraproject.org/wiki/Licensing:Main?rd=Licensing#Good_Licenses
# GPLv2: pcs
# BSD-2-Clause: pyagentx
License: GPLv2 and BSD-2-Clause
URL: https://github.com/ClusterLabs/pcs

# tar for unpacking pyagetx source tar ball
BuildRequires: tar

Requires: pcs = %{version}-%{release}
Requires: pacemaker
Requires: net-snmp

Provides: bundled(pyagentx) = %{pyagentx_version}

%description -n %{pcs_snmp_pkg_name}
SNMP agent that provides information about pacemaker cluster to the master agent (snmpd)

%prep
%autosetup -p1 -S git -n %{pcs_source_name}

# -- following is inspired by python-simplejon.el5 --
# Update timestamps on the files touched by a patch, to avoid non-equal
# .pyc/.pyo files across the multilib peers within a build

update_times(){
  # update_times <reference_file> <file_to_touch> ...
  # set the access and modification times of each file_to_touch to the times
  # of reference_file

  # put all args to file_list
  file_list=("$@")
  # first argument is reference_file: so take it and remove from file_list
  reference_file=${file_list[0]}
  unset file_list[0]

  for fname in ${file_list[@]}; do
    # some files could be deleted by a patch therefore we test file for
    # existance before touch to avoid exit with error: No such file or
    # directory
    # diffstat cannot create list of files without deleted files
    test -e $fname && touch -r $reference_file $fname
  done
}

update_times_patch(){
  # update_times_patch <patch_file_name>
  # set the access and modification times of each file in patch to the times
  # of patch_file_name

  patch_file_name=$1

  # diffstat
  # -l lists only the filenames. No histogram is generated.
  # -p override the logic that strips common pathnames,
  #    simulating the patch "-p" option. (Strip the smallest prefix containing
  #    num leading slashes from each file name found in the patch file)
  update_times ${patch_file_name} `diffstat -p1 -l ${patch_file_name}`
}

# update_times_patch %%{PATCH1}
update_times_patch %{PATCH1}
update_times_patch %{PATCH101}

cp -f %SOURCE1 pcsd/public/images
# prepare dirs/files necessary for building web ui
# inside SOURCE100 is only directory %%{ui_src_name}
tar -xzf %SOURCE100 -C %{pcsd_public_dir}
tar -xf %SOURCE101 -C %{pcsd_public_dir}/%{ui_src_name}

# prepare dirs/files necessary for building all bundles
# -----------------------------------------------------
# 1) configuration for rubygems
mkdir -p pcsd/.bundle
cp -f %SOURCE2 pcsd/.bundle/config

# 2) rubygems sources
mkdir -p pcsd/vendor/cache
cp -f %SOURCE81 pcsd/vendor/cache
cp -f %SOURCE82 pcsd/vendor/cache
cp -f %SOURCE83 pcsd/vendor/cache
cp -f %SOURCE84 pcsd/vendor/cache
cp -f %SOURCE86 pcsd/vendor/cache
# For reason why we are renaming open4 rubygem, see comment of source
# definition above.
cp -f %SOURCE87 pcsd/vendor/cache/open4-%{version_rubygem_open4}.gem
cp -f %SOURCE88 pcsd/vendor/cache
cp -f %SOURCE89 pcsd/vendor/cache
cp -f %SOURCE90 pcsd/vendor/cache
cp -f %SOURCE91 pcsd/vendor/cache
cp -f %SOURCE92 pcsd/vendor/cache
cp -f %SOURCE93 pcsd/vendor/cache
cp -f %SOURCE94 pcsd/vendor/cache
cp -f %SOURCE95 pcsd/vendor/cache
cp -f %SOURCE96 pcsd/vendor/cache
cp -f %SOURCE97 pcsd/vendor/cache
cp -f %SOURCE98 pcsd/vendor/cache


# 3) dir for python bundles
mkdir -p %{bundled_src_dir}

# 4) sources for pyagentx
tar -xzf %SOURCE41 -C %{bundled_src_dir}
mv %{bundled_src_dir}/pyagentx-%{pyagentx_version} %{bundled_src_dir}/pyagentx
update_times %SOURCE41 `find %{bundled_src_dir}/pyagentx -follow`
cp %{bundled_src_dir}/pyagentx/LICENSE.txt pyagentx_LICENSE.txt
cp %{bundled_src_dir}/pyagentx/CONTRIBUTORS.txt pyagentx_CONTRIBUTORS.txt
cp %{bundled_src_dir}/pyagentx/README.md pyagentx_README.md

# 5) sources for tornado
tar -xzf %SOURCE42 -C %{bundled_src_dir}
mv %{bundled_src_dir}/tornado-%{tornado_version} %{bundled_src_dir}/tornado
update_times %SOURCE42 `find %{bundled_src_dir}/tornado -follow`
cp %{bundled_src_dir}/tornado/LICENSE tornado_LICENSE
cp %{bundled_src_dir}/tornado/README.rst tornado_README.rst

# 7) sources for python dacite
tar -xzf %SOURCE44 -C %{bundled_src_dir}
mv %{bundled_src_dir}/dacite-%{dacite_version} %{bundled_src_dir}/dacite
update_times %SOURCE44 `find %{bundled_src_dir}/dacite -follow`
cp %{bundled_src_dir}/dacite/LICENSE dacite_LICENSE
cp %{bundled_src_dir}/dacite/README.md dacite_README.md

%build
%define debug_package %{nil}

%install
rm -rf $RPM_BUILD_ROOT
pwd

# build bundled rubygems (in main install it is disabled by BUILD_GEMS=false)
mkdir -p %{rubygem_bundle_dir}
# The '-g' cflags option is needed for generation of MiniDebugInfo for shared
# libraries from rubygem extensions
# Currently used rubygems with extensions: eventmachine, ffi, json, thin
# There was rpmdiff issue with missing .gnu_debugdata section
# see https://docs.engineering.redhat.com/display/HTD/rpmdiff-elf-stripping
gem install \
  --force --verbose --no-document -l --no-user-install \
  -i %{rubygem_bundle_dir} \
  %{rubygem_cache_dir}/backports-%{version_rubygem_backports}.gem \
  %{rubygem_cache_dir}/daemons-%{version_rubygem_daemons}.gem \
  %{rubygem_cache_dir}/ethon-%{version_rubygem_ethon}.gem \
  %{rubygem_cache_dir}/eventmachine-%{version_rubygem_eventmachine}.gem \
  %{rubygem_cache_dir}/ffi-%{version_rubygem_ffi}.gem \
  %{rubygem_cache_dir}/json-%{version_rubygem_json}.gem \
  %{rubygem_cache_dir}/mustermann-%{version_rubygem_mustermann}.gem \
  %{rubygem_cache_dir}/open4-%{version_rubygem_open4}.gem \
  %{rubygem_cache_dir}/rack-protection-%{version_rubygem_rack_protection}.gem \
  %{rubygem_cache_dir}/rack-test-%{version_rubygem_rack_test}.gem \
  %{rubygem_cache_dir}/rack-%{version_rubygem_rack}.gem \
  %{rubygem_cache_dir}/rexml-%{version_rubygem_rexml}.gem \
  %{rubygem_cache_dir}/ruby2_keywords-%{version_rubygem_ruby2_keywords}.gem \
  %{rubygem_cache_dir}/sinatra-%{version_rubygem_sinatra}.gem \
  %{rubygem_cache_dir}/thin-%{version_rubygem_thin}.gem \
  %{rubygem_cache_dir}/tilt-%{version_rubygem_tilt}.gem \
  %{rubygem_cache_dir}/webrick-%{version_rubygem_webrick}.gem \
  -- '--with-ldflags=%{build_ldflags}' \
     '--with-cflags=%{optflags}'

# prepare license files
# some rubygems do not have a license file (ruby2_keywords, thin)
mv %{rubygem_bundle_dir}/gems/backports-%{version_rubygem_backports}/LICENSE.txt backports_LICENSE.txt
mv %{rubygem_bundle_dir}/gems/daemons-%{version_rubygem_daemons}/LICENSE daemons_LICENSE
mv %{rubygem_bundle_dir}/gems/ethon-%{version_rubygem_ethon}/LICENSE ethon_LICENSE
mv %{rubygem_bundle_dir}/gems/eventmachine-%{version_rubygem_eventmachine}/LICENSE eventmachine_LICENSE
mv %{rubygem_bundle_dir}/gems/eventmachine-%{version_rubygem_eventmachine}/GNU eventmachine_GNU
mv %{rubygem_bundle_dir}/gems/ffi-%{version_rubygem_ffi}/COPYING ffi_COPYING
mv %{rubygem_bundle_dir}/gems/ffi-%{version_rubygem_ffi}/LICENSE ffi_LICENSE
mv %{rubygem_bundle_dir}/gems/ffi-%{version_rubygem_ffi}/LICENSE.SPECS ffi_LICENSE.SPECS
mv %{rubygem_bundle_dir}/gems/json-%{version_rubygem_json}/LICENSE json_LICENSE
mv %{rubygem_bundle_dir}/gems/mustermann-%{version_rubygem_mustermann}/LICENSE mustermann_LICENSE
mv %{rubygem_bundle_dir}/gems/open4-%{version_rubygem_open4}/LICENSE open4_LICENSE
mv %{rubygem_bundle_dir}/gems/rack-%{version_rubygem_rack}/MIT-LICENSE rack_MIT-LICENSE
mv %{rubygem_bundle_dir}/gems/rexml-%{version_rubygem_rexml}/LICENSE.txt rexml_LICENSE.txt
mv %{rubygem_bundle_dir}/gems/rack-protection-%{version_rubygem_rack_protection}/License rack-protection_License
mv %{rubygem_bundle_dir}/gems/rack-test-%{version_rubygem_rack_test}/MIT-LICENSE.txt rack-test_MIT-LICENSE.txt
mv %{rubygem_bundle_dir}/gems/sinatra-%{version_rubygem_sinatra}/LICENSE sinatra_LICENSE
mv %{rubygem_bundle_dir}/gems/tilt-%{version_rubygem_tilt}/COPYING tilt_COPYING
mv %{rubygem_bundle_dir}/gems/webrick-%{version_rubygem_webrick}/LICENSE.txt webrick_LICENSE.txt

# build web ui and put it to pcsd
make -C %{pcsd_public_dir}/%{ui_src_name} build
mv %{pcsd_public_dir}/%{ui_src_name}/build  pcsd/public/ui
rm -r %{pcsd_public_dir}/%{ui_src_name}

# main pcs install
%make_install \
  PREFIX=%{_prefix} \
  SYSTEMD_UNIT_DIR=%{_unitdir} \
  LIB_DIR=%{pcs_libdir} \
  PYTHON=%{__python3} \
  PYTHON_SITELIB=%{python3_sitelib} \
  BASH_COMPLETION_DIR=%{_datadir}/bash-completion/completions \
  BUNDLE_PYAGENTX_SRC_DIR=`readlink -f %{bundled_src_dir}/pyagentx` \
  BUNDLE_TORNADO_SRC_DIR=`readlink -f %{bundled_src_dir}/tornado` \
  BUNDLE_DACITE_SRC_DIR=`readlink -f %{bundled_src_dir}/dacite` \
  BUILD_GEMS=false \
  SYSTEMCTL_OVERRIDE=true \
  hdrdir="%{_includedir}" \
  rubyhdrdir="%{_includedir}" \
  includedir="%{_includedir}"

# symlink favicon into pcsd directories
ln -fs /etc/favicon.png ${RPM_BUILD_ROOT}%{pcs_libdir}/%{pcsd_public_dir}/images/favicon.png

#after the ruby gem compilation we do not need ruby gems in the cache
rm -r -v $RPM_BUILD_ROOT%{pcs_libdir}/%{rubygem_cache_dir}

# We are not building debug package for pcs but we need to add MiniDebuginfo
# to the bundled shared libraries from rubygem extensions in order to satisfy
# rpmdiff's binary stripping checker.
# Therefore we call find-debuginfo.sh script manually in order to strip
# binaries and add MiniDebugInfo with .gnu_debugdata section
/usr/lib/rpm/find-debuginfo.sh -j2 -m -i -S debugsourcefiles.list
# find-debuginfo.sh generated some files into /usr/lib/debug  and
# /usr/src/debug/ that we don't want in the package
rm -rf $RPM_BUILD_ROOT%{pcs_libdir}/debug
rm -rf $RPM_BUILD_ROOT%{_prefix}/src/debug

# We can remove files required for gem compilation
rm -rf $RPM_BUILD_ROOT%{pcs_libdir}/%{rubygem_bundle_dir}/gems/eventmachine-%{version_rubygem_eventmachine}/ext
rm -rf $RPM_BUILD_ROOT%{pcs_libdir}/%{rubygem_bundle_dir}/gems/ffi-%{version_rubygem_ffi}/ext
rm -rf $RPM_BUILD_ROOT%{pcs_libdir}/%{rubygem_bundle_dir}/gems/json-%{version_rubygem_json}/ext
rm -rf $RPM_BUILD_ROOT%{pcs_libdir}/%{rubygem_bundle_dir}/gems/thin-%{version_rubygem_thin}/ext

%check
# In the building environment LC_CTYPE is set to C which causes tests to fail
# due to python prints a warning about it to stderr. The following environment
# variable disables the warning.
# On the live system either UTF8 locale is set or the warning is emmited
# which breaks pcs. That is the correct behavior since with wrong locales it
# would be probably broken anyway.
# The main concern here is to make the tests pass.
# See https://fedoraproject.org/wiki/Changes/python3_c.utf-8_locale for details.
export PYTHONCOERCECLOCALE=0

run_all_tests(){
  #run pcs tests

  # disabled tests:
  #
  # pcs_test.tier0.lib.commands.test_resource_agent.DescribeAgentUtf8.test_describe
  #   For an unknown reason this test is failing in mock environment and
  #   passing outside the mock environment.
  #   TODO: Investigate the issue

  BUNDLED_LIB_LOCATION=$RPM_BUILD_ROOT%{pcs_libdir}/pcs/bundled/packages \
    %{__python3} pcs_test/suite.py --tier0 -v --vanilla --all-but \
    pcs_test.tier0.lib.commands.test_resource_agent.DescribeAgentUtf8.test_describe \
    pcs_test.tier0.daemon.app.test_app_remote.SyncConfigMutualExclusive.test_get_not_locked \
    pcs_test.tier0.daemon.app.test_app_remote.SyncConfigMutualExclusive.test_post_not_locked \

  test_result_python=$?

  #run pcsd tests and remove them
  pcsd_dir=$RPM_BUILD_ROOT%{pcs_libdir}/pcsd
  GEM_HOME=$RPM_BUILD_ROOT%{pcs_libdir}/%{rubygem_bundle_dir} ruby \
    -I${pcsd_dir} \
    -I${pcsd_dir}/test \
    ${pcsd_dir}/test/test_all_suite.rb
  test_result_ruby=$?

  if [ $test_result_python -ne 0 ]; then
    return $test_result_python
  fi
  return $test_result_ruby
}

remove_all_tests() {
  pcsd_dir=$RPM_BUILD_ROOT%{pcs_libdir}/pcsd
  #remove pcsd tests, we do not distribute them in the rpm
  rm -r -v ${pcsd_dir}/test

  # remove javascript testing files
  rm -r -v ${pcsd_dir}/public/js/dev
}

run_all_tests
remove_all_tests

%posttrans
# Make sure the new version of the daemon is runnning.
# Also, make sure to start pcsd-ruby if it hasn't been started or even
# installed before. This is done by restarting pcsd.service.
%{_bindir}/systemctl daemon-reload
%{_bindir}/systemctl try-restart pcsd.service


%post
%systemd_post pcsd.service
%systemd_post pcsd-ruby.service

%post -n %{pcs_snmp_pkg_name}
%systemd_post pcs_snmp_agent.service

%preun
%systemd_preun pcsd.service
%systemd_preun pcsd-ruby.service

%preun -n %{pcs_snmp_pkg_name}
%systemd_preun pcs_snmp_agent.service

%postun
%systemd_postun_with_restart pcsd.service
%systemd_postun_with_restart pcsd-ruby.service

%postun -n %{pcs_snmp_pkg_name}
%systemd_postun_with_restart pcs_snmp_agent.service

%files
%doc CHANGELOG.md
%doc README.md
%doc tornado_README.rst
%doc dacite_README.md
%license tornado_LICENSE
%license dacite_LICENSE
%license COPYING
# rugygem licenses
%license backports_LICENSE.txt
%license daemons_LICENSE
%license ethon_LICENSE
%license eventmachine_LICENSE
%license eventmachine_GNU
%license ffi_COPYING
%license ffi_LICENSE
%license ffi_LICENSE.SPECS
%license json_LICENSE
%license mustermann_LICENSE
%license open4_LICENSE
%license rack_MIT-LICENSE
%license rack-protection_License
%license rack-test_MIT-LICENSE.txt
%license rexml_LICENSE.txt
%license sinatra_LICENSE
%license tilt_COPYING
%license webrick_LICENSE.txt
%{python3_sitelib}/pcs
%{python3_sitelib}/pcs-%{version}-py3.*.egg-info
%{_sbindir}/pcs
%{_sbindir}/pcsd
%{pcs_libdir}/pcs/pcs_internal
%{pcs_libdir}/pcsd/*
%{pcs_libdir}/pcsd/.bundle/config
%{pcs_libdir}/pcs/bundled/packages/tornado*
%{pcs_libdir}/pcs/bundled/packages/dacite*
%{_unitdir}/pcsd.service
%{_unitdir}/pcsd-ruby.service
%{_datadir}/bash-completion/completions/pcs
%{_sharedstatedir}/pcsd
%config(noreplace) %{_sysconfdir}/pam.d/pcsd
%dir %{_var}/log/pcsd
%config(noreplace) %{_sysconfdir}/logrotate.d/pcsd
%config(noreplace) %{_sysconfdir}/sysconfig/pcsd
%ghost %config(noreplace) %attr(0600,root,root) %{_sharedstatedir}/pcsd/cfgsync_ctl
%ghost %config(noreplace) %attr(0600,root,root) %{_sharedstatedir}/pcsd/known-hosts
%ghost %config(noreplace) %attr(0600,root,root) %{_sharedstatedir}/pcsd/pcsd.cookiesecret
%ghost %config(noreplace) %attr(0600,root,root) %{_sharedstatedir}/pcsd/pcsd.crt
%ghost %config(noreplace) %attr(0600,root,root) %{_sharedstatedir}/pcsd/pcsd.key
%ghost %config(noreplace) %attr(0644,root,root) %{_sharedstatedir}/pcsd/pcs_settings.conf
%ghost %config(noreplace) %attr(0644,root,root) %{_sharedstatedir}/pcsd/pcs_users.conf
%{_mandir}/man8/pcs.*
%{_mandir}/man8/pcsd.*
%exclude %{pcs_libdir}/pcsd/*.debian
%exclude %{pcs_libdir}/pcsd/pcsd.service
%exclude %{pcs_libdir}/pcsd/pcsd-ruby.service
%exclude %{pcs_libdir}/pcsd/pcsd.conf
%exclude %{pcs_libdir}/pcsd/pcsd.8
%exclude %{pcs_libdir}/pcsd/public/js/dev/*
%exclude %{pcs_libdir}/pcsd/Gemfile
%exclude %{pcs_libdir}/pcsd/Gemfile.lock
%exclude %{pcs_libdir}/pcsd/Makefile
%exclude %{python3_sitelib}/pcs/bash_completion
%exclude %{python3_sitelib}/pcs/pcs.8
%exclude %{python3_sitelib}/pcs/pcs

%files -n %{pcs_snmp_pkg_name}
%{pcs_libdir}/pcs/pcs_snmp_agent
%{pcs_libdir}/pcs/bundled/packages/pyagentx*
%{_unitdir}/pcs_snmp_agent.service
%{_datadir}/snmp/mibs/PCMK-PCS*-MIB.txt
%{_mandir}/man8/pcs_snmp_agent.*
%config(noreplace) %{_sysconfdir}/sysconfig/pcs_snmp_agent
%doc CHANGELOG.md
%doc pyagentx_CONTRIBUTORS.txt
%doc pyagentx_README.md
%license COPYING
%license pyagentx_LICENSE.txt

%changelog
* Wed Apr 28 2021 Miroslav Lisik <mlisik@redhat.com> - 0.10.8-5
- Updated pcs web ui node modules
- Fixed build issue on low memory build hosts
- Resolves: rhbz#1951272

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 0.10.8-4
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Thu Mar 04 2021 Miroslav Lisik <mlisik@redhat.com> - 0.10.8-3
- Replace pyOpenSSL with python-cryptography
- Resolves: rhbz#1927404

* Fri Feb 19 2021 Miroslav Lisik <mlisik@redhat.com> - 0.10.8-2
- Bundle rubygem depedencies and python3-tornado
- Resolves: rhbz#1929710

* Thu Feb 04 2021 Miroslav Lisik <mlisik@redhat.com> - 0.10.8-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- Updated pcs-web-ui
- Updated bundled python dependency: dacite
- Changed BuildRequires from git to git-core
- Added conditional (Build)Requires: rubygem(rexml)
- Added conditional Requires: rubygem(webrick)

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 0.10.7-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Thu Jan  7 2021 Vít Ondruch <vondruch@redhat.com> - 0.10.7-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/Ruby_3.0

* Thu Nov 26 2020 Ondrej Mular <omular@redhat.com> - 0.10.7-2
- Python 3.10 related fix

* Wed Sep 30 2020 Miroslav Lisik <mlisik@redhat.com> - 0.10.7-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- Added dependency on python packages pyparsing and dateutil
- Fixed virtual bundle provides for ember, handelbars, jquery and jquery-ui
- Removed dependency on python3-clufter

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 0.10.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 21 2020 Miroslav Lisik <mlisik@redhat.com> - 0.10.6-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- Updated pcs-web-ui
- Stopped bundling tornado (use distribution package instead)
- Stopped bundling rubygem-tilt (use distribution package instead)
- Removed rubygem bundling
- Removed unneeded BuildRequires: execstack, gcc, gcc-c++
- Excluded some tests for tornado daemon

* Tue Jul 21 2020 Tom Stellard <tstellar@redhat.com> - 0.10.5-8
- Use make macros
- https://fedoraproject.org/wiki/Changes/UseMakeBuildInstallMacro

* Wed Jul 15 2020 Ondrej Mular <omular@redhat.com> - 0.10.5-7
- Use fixed upstream version of dacite with Python 3.9 support
- Split upstream tests in gating into tiers

* Fri Jul 03 2020 Ondrej Mular <omular@redhat.com> - 0.10.5-6
- Use patched version of dacite compatible with Python 3.9
- Resolves: rhbz#1838327

* Tue May 26 2020 Miro Hrončok <mhroncok@redhat.com> - 0.10.5-5
- Rebuilt for Python 3.9

* Thu May 07 2020 Ondrej Mular <omular@redhat.com> - 0.10.5-4
- Rebased to latest upstream sources (see CHANGELOG.md)
- Run only tier0 tests in check section

* Fri Apr 03 2020 Ondrej Mular <omular@redhat.com> - 0.10.5-3
- Enable gating

* Fri Mar 27 2020 Ondrej Mular <omular@redhat.com> - 0.10.5-2
- Remove usage of deprecated module xml.etree.cElementTree
- Resolves: rhbz#1817695

* Wed Mar 18 2020 Miroslav Lisik <mlisik@redhat.com> - 0.10.5-1
- Rebased to latest upstream sources (see CHANGELOG.md)

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 0.10.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Thu Nov 28 2019 Miroslav Lisik <mlisik@redhat.com> - 0.10.4-1
- Rebased to latest upstream sources (see CHANGELOG.md)

* Thu Oct 03 2019 Miro Hrončok <mhroncok@redhat.com> - 0.10.3-2
- Rebuilt for Python 3.8.0rc1 (#1748018)

* Fri Aug 23 2019 Ondrej Mular <omular@redhat.com> - 0.10.3-1
- Rebased to latest upstream sources (see CHANGELOG.md)

* Mon Aug 19 2019 Miro Hrončok <mhroncok@redhat.com> - 0.10.2-3
- Rebuilt for Python 3.8

* Fri Jul 26 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.10.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Fri Jun 14 2019 Ondrej Mular <omular@redhat.com> - 0.10.2-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- Added pam as required package
- An alternative webUI rebased to latest upstream sources
- Improved configuration files permissions in rpm

* Tue Mar 19 2019 Tomas Jelinek <tojeline@redhat.com> - 0.10.1-4
- Removed unused dependency rubygem-multi_json
- Removed files needed only for building rubygems from the package

* Mon Feb 04 2019 Ivan Devát <idevat@redhat.com> - 0.10.1-3
- Corrected gem install flags

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.10.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Wed Jan 09 2019 Ivan Devát <idevat@redhat.com> - 0.10.1-1
- Rebased to latest upstream sources (see CHANGELOG.md)

* Tue Oct 09 2018 Ondrej Mular <omular@redhat.com> - 0.10.0.alpha.6-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- Resolves: rhbz#1618911

* Fri Aug 31 2018 Ivan Devát <idevat@redhat.com> - 0.10.0.alpha.2-3
- Started bundling rubygem-tilt (rubygem-tilt is orphaned in fedora due to rubygem-prawn dependency)
- Enabled passing tests

* Sat Aug 25 2018 Ivan Devát <idevat@redhat.com> - 0.10.0.alpha.2-2
- Fixed error with missing rubygem location during pcsd start
- Resolves: rhbz#1618911

* Thu Aug 02 2018 Ivan Devát <idevat@redhat.com> - 0.10.0.alpha.2-1
- Rebased to latest upstream sources (see CHANGELOG.md)

* Wed Jul 25 2018 Ivan Devát <idevat@redhat.com> - 0.9.164-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.164-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 0.9.164-2
- Rebuilt for Python 3.7

* Mon Apr 09 2018 Ondrej Mular <omular@redhat.com> - 0.9.164-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- Fixed: CVE-2018-1086, CVE-2018-1079

* Mon Feb 26 2018 Ivan Devát <idevat@redhat.com> - 0.9.163-2
- Fixed crash when adding a node to a cluster

* Tue Feb 20 2018 Ivan Devát <idevat@redhat.com> - 0.9.163-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- Adapted for Rack 2 and Sinatra 2

* Fri Feb 09 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 0.9.160-5
- Escape macros in %%changelog

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.160-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sat Jan 20 2018 Björn Esser <besser82@fedoraproject.org> - 0.9.160-3
- Rebuilt for switch to libxcrypt

* Fri Jan 05 2018 Mamoru TASAKA <mtasaka@fedoraproject.org> - 0.9.160-2
- F-28: rebuild for ruby25
- Workaround for gem install option

* Wed Oct 18 2017 Ondrej Mular <omular@redhat.com> - 0.9.160-1
- Rebased to latest upstream sources (see CHANGELOG.md)
- All pcs tests are temporarily disabled because of issues in pacemaker.

* Thu Sep 14 2017 Ondrej Mular <omular@redhat.com> - 0.9.159-4
- Bundle rubygem-rack-protection which is being updated to 2.0.0 in Fedora.
- Removed setuptools patch.
- Disabled debuginfo subpackage.

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.159-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.159-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Jul 12 2017 Ondrej Mular <omular@redhat.com> - 0.9.159-1
- Rebased to latest upstream sources (see CHANGELOG.md)

* Tue May 23 2017 Tomas Jelinek <tojeline@redhat.com> - 0.9.156-3
- Fixed python locales issue preventing build-time tests to pass
- Bundle rubygem-tilt which is being retired from Fedora

* Thu Mar 23 2017 Tomas Jelinek <tojeline@redhat.com> - 0.9.156-2
- Fixed Cross-site scripting (XSS) vulnerability in web UI CVE-2017-2661
- Re-added support for clufter as it is now available for Python 3

* Wed Feb 22 2017 Tomas Jelinek <tojeline@redhat.com> - 0.9.156-1
- Rebased to latest upstream sources (see CHANGELOG.md)

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.155-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Jan 12 2017 Vít Ondruch <vondruch@redhat.com> - 0.9.155-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Ruby_2.4

* Wed Jan 04 2017 Adam Williamson <awilliam@redhat.com> - 0.9.155-1
- Latest release 0.9.155
- Fix tests with Python 3.6 and lxml 3.7
- Package the license as license, not doc
- Use -f param for rm when wiping test directories as they are nested now

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com>
- Rebuild for Python 3.6

* Tue Oct 18 2016 Tomas Jelinek <tojeline@redhat.com> - 0.9.154-2
- Fixed upgrading from pcs-0.9.150

* Thu Sep 22 2016 Tomas Jelinek <tojeline@redhat.com> - 0.9.154-1
- Re-synced to upstream sources
- Spec file cleanup and fixes

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.150-2
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Mon Apr 11 2016 Tomas Jelinek <tojeline@redhat.com> - 0.9.150-1
- Re-synced to upstream sources
- Make pcs depend on python3
- Spec file cleanup

* Tue Feb 23 2016 Tomas Jelinek <tojeline@redhat.com> - 0.9.149-2
- Fixed rubygems issues which prevented pcsd from starting
- Added missing python-lxml dependency

* Thu Feb 18 2016 Tomas Jelinek <tojeline@redhat.com> - 0.9.149-1
- Re-synced to upstream sources
- Security fix for CVE-2016-0720, CVE-2016-0721
- Fixed rubygems issues which prevented pcsd from starting
- Rubygems built with RELRO
- Spec file cleanup
- Fixed multilib .pyc/.pyo issue

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.144-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Jan 12 2016 Vít Ondruch <vondruch@redhat.com> - 0.9.144-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Ruby_2.3

* Fri Sep 18 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.144-1
- Re-synced to upstream sources

* Tue Jun 23 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.141-2
- Added requirement for psmisc for killall

* Tue Jun 23 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.141-1
- Re-synced to upstream sources

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.140-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Fri Jun 05 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.140-1
- Re-synced to upstream sources

* Fri May 22 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.139-4
- Fix for CVE-2015-1848, CVE-2015-3983 (sessions not signed)

* Thu Mar 26 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.139-3
- Add BuildRequires: systemd (rhbz#1206253)

* Fri Feb 27 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.139-2
- Reflect clufter inclusion (rhbz#1180723)

* Thu Feb 19 2015 Tomas Jelinek <tojeline@redhat.com> - 0.9.139-1
- Re-synced to upstream sources

* Sat Jan 17 2015 Mamoru TASAKA <mtasaka@fedoraproject.org> - 0.9.115-5
- Rebuild for https://fedoraproject.org/wiki/Changes/Ruby_2.2

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.115-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Fri Jun 06 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.115-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue May 06 2014 Tomas Jelinek <tojeline@redhat.com> - 0.9.115-2
- Rebuild to fix ruby dependencies

* Mon Apr 21 2014 Chris Feist <cfeist@redhat.com> - 0.9.115-1
- Re-synced to upstream sources

* Fri Dec 13 2013 Chris Feist <cfeist@redhat.com> - 0.9.102-1
- Re-synced to upstream sources

* Wed Jun 19 2013 Chris Feist <cfeist@redhat.com> - 0.9.48-1
- Rebuild with upstream sources

* Thu Jun 13 2013 Chris Feist <cfeist@redhat.com> - 0.9.44-5
- Added fixes for building rpam with ruby-2.0.0

* Mon Jun 03 2013 Chris Feist <cfeist@redhat.com> - 0.9.44-4
- Rebuild with upstream sources

* Tue May 07 2013 Chris Feist <cfeist@redhat.com> - 0.9.41-2
- Resynced to upstream sources

* Fri Apr 19 2013 Chris Feist <cfeist@redhat.com> - 0.9.39-1
- Fixed gem building
- Re-synced to upstream sources

* Mon Mar 25 2013 Chris Feist <cfeist@rehdat.com> - 0.9.36-4
- Don't try to build gems at all

* Mon Mar 25 2013 Chris Feist <cfeist@rehdat.com> - 0.9.36-3
- Removed all gems from build, will need to find pam package in the future

* Mon Mar 25 2013 Chris Feist <cfeist@redhat.com> - 0.9.36-2
- Removed duplicate libraries already present in fedora

* Mon Mar 18 2013 Chris Feist <cfeist@redhat.com> - 0.9.36-1
- Resynced to latest upstream

* Mon Mar 11 2013 Chris Feist <cfeist@redhat.com> - 0.9.33-1
- Resynched to latest upstream
- pcsd has been moved to /usr/lib to fix /usr/local packaging issues

* Thu Feb 21 2013 Chris Feist <cfeist@redhat.com> - 0.9.32-1
- Resynced to latest version of pcs/pcsd

* Mon Nov 05 2012 Chris Feist <cfeist@redhat.com> - 0.9.27-3
- Build on all archs

* Thu Oct 25 2012 Chris Feist <cfeist@redhat.com> - 0.9.27-2
- Resync to latest version of pcs
- Added pcsd daemon

* Mon Oct 08 2012 Chris Feist <cfeist@redhat.cmo> - 0.9.26-1
- Resync to latest version of pcs

* Thu Sep 20 2012 Chris Feist <cfeist@redhat.cmo> - 0.9.24-1
- Resync to latest version of pcs

* Thu Sep 20 2012 Chris Feist <cfeist@redhat.cmo> - 0.9.23-1
- Resync to latest version of pcs

* Wed Sep 12 2012 Chris Feist <cfeist@redhat.cmo> - 0.9.22-1
- Resync to latest version of pcs

* Thu Sep 06 2012 Chris Feist <cfeist@redhat.cmo> - 0.9.19-1
- Resync to latest version of pcs

* Tue Aug 07 2012 Chris Feist <cfeist@redhat.com> - 0.9.12-1
- Resync to latest version of pcs

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu May 24 2012 Chris Feist <cfeist@redhat.com> - 0.9.4-1
- Resync to latest version of pcs
- Move cluster creation options to cluster sub command.

* Mon May 07 2012 Chris Feist <cfeist@redhat.com> - 0.9.3.1-1
- Resync to latest version of pcs which includes fixes to work with F17.

* Mon Mar 19 2012 Chris Feist <cfeist@redhat.com> - 0.9.2.4-1
- Resynced to latest version of pcs

* Mon Jan 23 2012 Chris Feist <cfeist@redhat.com> - 0.9.1-1
- Updated BuildRequires and %%doc section for fedora

* Fri Jan 20 2012 Chris Feist <cfeist@redhat.com> - 0.9.0-2
- Updated spec file for fedora specific changes

* Mon Jan 16 2012 Chris Feist <cfeist@redhat.com> - 0.9.0-1
- Initial Build
